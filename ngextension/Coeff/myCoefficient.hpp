#ifndef __FILE_MYCOEFFICIENT_HPP
#define __FILE_MYCOEFFICIENT_HPP

namespace ngfem
{
  

  class ReferenceSolution : public CoefficientFunction
  {
  public:
    ReferenceSolution()
      : CoefficientFunction(/*dimension = */ 1,true) { ; }

    // ********************* Necessary **********************************
    virtual double Evaluate(const BaseMappedIntegrationPoint& mip) const override
    {
      return 0.0;
    }

    virtual Complex EvaluateComplex(const BaseMappedIntegrationPoint& mip) const override
     {
      auto pnt = mip.GetPoint();
      double r = sqrt(pnt[0]*pnt[0]+pnt[1]*pnt[1]);
      double omega = 32;
      double a = 0.5;
      double phi = atan2(pnt[1],pnt[0]);
      Complex tmp1 = cyl_bessel_j(1,omega*r) + Complex(0,1)*cyl_neumann(1,omega*r);
      Complex tmp2 = 0.5*omega*(  (cyl_bessel_j(0,omega*a) -  cyl_bessel_j(2,omega*a)) + Complex(0,1)*( cyl_neumann(0,omega*a) - cyl_neumann(2,omega*a)) )  ;
      // return 99.99;
      return (tmp1/tmp2)*exp(Complex(0,1)*1*phi);
     }

    virtual void Evaluate(const BaseMappedIntegrationPoint& mip, FlatVector<Complex> result) const override
    {
	    Complex f = EvaluateComplex(mip);
	    result(0) = f;
    }

  };

  void ExportReferenceSolution (py::module m);
  
  class FullSolution : public CoefficientFunction
  {
  public:
    
    double omega; // wavenumber
    double a; // radius of scattering object

    FullSolution(double aomega,double aa) : CoefficientFunction(/*dimension = */ 1,true) { 
      omega = aomega;
      a = aa;        
    }

    //FullSolution()
    //  : CoefficientFunction(/*dimension = */ 1,true) { ; }

    // ********************* Necessary **********************************
    virtual double Evaluate(const BaseMappedIntegrationPoint& mip) const override
    {
      return 0.0;
    }

    virtual Complex EvaluateComplex(const BaseMappedIntegrationPoint& mip) const override
     {
      auto pnt = mip.GetPoint();
      double r = sqrt(pnt[0]*pnt[0]+pnt[1]*pnt[1]);
      double phi = atan2(pnt[1],pnt[0]);

      //Complex tmp1 = cyl_bessel_j(1,omega*r) + Complex(0,1)*cyl_neumann(1,omega*r);
      //Complex tmp2 = 0.5*omega*(  (cyl_bessel_j(0,omega*a) -  cyl_bessel_j(2,omega*a)) + Complex(0,1)*( cyl_neumann(0,omega*a) - cyl_neumann(2,omega*a)) )  ;
      // return 99.99;
      //return (tmp1/tmp2)*exp(Complex(0,1)*1*phi);
      
      Complex result = 0.0; 

      // compute n = 0 term
      Complex tmp1 = -cyl_bessel_j(1,omega*a);
      tmp1 *= ( cyl_bessel_j(0,omega*r) + Complex(0,1)*cyl_neumann(0,omega*r) );
      Complex tmp2 = ( cyl_bessel_j(1,omega*a) + Complex(0,1)*cyl_neumann(1,omega*a));
      result += tmp1/tmp2;
      
      // compute n >= 1 terms 
      int n = 1; 
      double tol = 1e-12;
      double mag = 1;
      while (n < 100 && mag > tol) {
	      tmp1 = (cyl_bessel_j(n-1,omega*a) - cyl_bessel_j(n+1,omega*a))*( cyl_bessel_j(n,omega*r) + Complex(0,1)* cyl_neumann(n,omega*r) );
              tmp2 = (cyl_bessel_j(n-1,omega*a) - cyl_bessel_j(n+1,omega*a)) + Complex(0,1)*(cyl_neumann(n-1,omega*a) - cyl_neumann(n+1,omega*a));
	      Complex term_n = tmp1/tmp2;
	      mag = abs(term_n);
	      //cout << "mag = " << mag << endl;
	      result -= 2*pow(Complex(0,1),n)*term_n*cos(n*phi);
	      n++;  
      } 
      return result; 

     }

    virtual void Evaluate(const BaseMappedIntegrationPoint& mip, FlatVector<Complex> result) const override
    {
	    Complex f = EvaluateComplex(mip);
	    result(0) = f;
    }

  };

  void ExportFullSolution (py::module m);


  // KiteSolution

  class KiteSolution : public CoefficientFunction
  {
   
  private:
    
    Vector<double> X (double t) const 
    {
      Vector<double> result = {cos(t)+0.65*cos(2*t)-0.65,1.5*sin(t)}; 
      return result; 
    }

    Vector<double> dX(double t) const 
    {
      Vector<double> result = {-sin(t)-1.3*sin(2*t),1.5*cos(t)}; 
      return result; 
    }
    
    Vector<double> ddX(double t) const
    {
      Vector<double> result = {-cos(t)-2.6*cos(2*t),-1.5*sin(t)}; 
      return result; 
    }

    double r(double t, double tau) const {
      return L2Norm(X(t)-X(tau));
    }

    double dX_r(double t) const {
      return L2Norm(dX(t));
    }

    const double euler_gamma = 0.577215664901532860606512090082;

    Complex hankel1(int n,double x) const {
      return cyl_bessel_j(n,x)+Complex(0,1)*cyl_neumann(n,x);
    }
 
    Complex d_hankel1_order0(double x) const {
      return -hankel1(1,x);
    }

    Complex dd_hankel1_order0(double x) const {
      if (abs(x) > 1e-14) {
        return -hankel1(0,x) +(1/x)*hankel1(1,x);
      } 
      else {
	throw Exception("Division by zero in dd_hankel1_order0!"); 
      }
    }

    Complex L1(double kappa,double t, double tau) {
      if (t == tau){
        return 0.0;
      }
      else { 
        Complex tmp = (kappa/(2*M_PI))*(cyl_bessel_j(1,kappa*r(t,tau))/r(t,tau));
        return tmp*( dX(tau)(1)*( X(t)(0)-X(tau)(0)) - dX(tau)(0)*( X(t)(1)-X(tau)(1)) );      
      }
    }

    Complex L2(double kappa, double t, double tau) { 
      if(t==tau){
        return (1/(2*M_PI))*(dX(t)(0)*ddX(t)(1)-dX(t)(1)*ddX(t)(0))/pow(dX_r(t),2) ;
      }
      else {
        Complex M = (0.5*kappa*Complex(0,1))*(hankel1(1,kappa*r(t,tau))/r(t,tau))*( dX(tau)(1)*(X(tau)(0)-X(t)(0)) - dX(tau)(0)*(X(tau)(1)-X(t)(1))  )  ;  
        return M - L1(kappa,t,tau)*log(4*pow(sin(0.5*(t-tau)),2));  
      }
    }

    Complex M1(double kappa, double t, double tau) {
      return -(1/(2*M_PI))*cyl_bessel_j(0,kappa*r(t,tau))*dX_r(tau);
    }

    Complex M2(double kappa, double t, double tau) {
      if (t == tau) {
        return (0.5*Complex(0,1) - euler_gamma/M_PI - (1/M_PI)*log(kappa*0.5*dX_r(t)))*dX_r(t);  
      }
      else {
        return 0.5*Complex(0,1)*hankel1(0,kappa*r(t,tau))*dX_r(tau) - M1(kappa,t,tau)*log(4*pow(sin(0.5*(t-tau)),2));
      }
    }

    Complex K1(double kappa, double eta, double t, double tau) {
      return L1(kappa,t,tau) + Complex(0,1)*eta*M1(kappa,t,tau);
    }
    
    Complex K2(double kappa, double eta, double t, double tau) {
      return L2(kappa,t,tau) + Complex(0,1)*eta*M2(kappa,t,tau);
    }

    double RR(double t,vector<double> ts,int j,int n) {
      double result = 0.0;
      for(int m = 1; m < n; m++) {
        result -= cos(m*(t-ts[j]))/m;
      }
      result *= 2*M_PI/n;
      return result - (M_PI/pow(n,2))*cos(n*(t-ts[j]));
    }

    Complex PHI(double kappa,Vector<double> x, Vector<double> y) const {
      return 0.25*Complex(0,1)*(hankel1(0,kappa*L2Norm(x-y)));  
    }
   
    Vector<Complex> dy_PHI(double kappa,Vector<double> x, Vector<double> y) const {
      Complex prefac = -0.25*Complex(0,1)*kappa*(cyl_bessel_j(1,kappa* L2Norm(x-y)) + Complex(0,1)*cyl_neumann(1,kappa*L2Norm(x-y)))/L2Norm(x-y);  
      return prefac*(y-x);
    }
    
    Vector<Complex> dX_PHI(double kappa,Vector<double> x, Vector<double> y) const {
      return -dy_PHI(kappa,x,y); 
    }

   
    Complex dx0dy0_PHI(double kappa,Vector<double> x, Vector<double> y) const { 
      Complex a = dd_hankel1_order0(kappa*L2Norm(x-y))*kappa*pow(y(0)-x(0),2)/pow(L2Norm(x-y),2);  
      Complex b = d_hankel1_order0(kappa*L2Norm(x-y))*pow(x(1)-y(1),2)/pow(L2Norm(x-y),3);
      return -Complex(0,1)*kappa*0.25*(a+b);
    }
    
    Complex dx1dy1_PHI(double kappa,Vector<double> x, Vector<double> y) const { 
      Complex a = dd_hankel1_order0(kappa*L2Norm(x-y))*kappa*pow(y(1)-x(1),2)/pow(L2Norm(x-y),2);  
      Complex b = d_hankel1_order0(kappa*L2Norm(x-y))*pow(x(0)-y(0),2)/pow(L2Norm(x-y),3);
      return -Complex(0,1)*kappa*0.25*(a+b);
    }

    Complex dx1dy0_PHI(double kappa,Vector<double> x, Vector<double> y) const { 
      Complex a = dd_hankel1_order0(kappa*L2Norm(x-y))*kappa*(x(1)-y(1))*(y(0)-x(0))  /pow(L2Norm(x-y),2);  
      Complex b = d_hankel1_order0(kappa*L2Norm(x-y))*(x(0)-y(0))*(x(1)-y(1))/pow(L2Norm(x-y),3);
      return Complex(0,1)*kappa*0.25*(a+b);
    }

    Complex dx0dy1_PHI(double kappa,Vector<double> x, Vector<double> y) const { 
      return dx1dy0_PHI(kappa,x,y); 
    }

    Complex dnx_PHI(double kappa,Vector<double> x, Vector<double> y,Vector<double> nx) const { 
      return  InnerProduct(dX_PHI(kappa,x,y),nx);
    }
    
    Complex dnxdny_PHI(double kappa,Vector<double> x, Vector<double> y,Vector<double> nx,Vector<double> ny) const { 
      return dx0dy0_PHI(kappa,x,y)*nx(0)*ny(0) + dx0dy1_PHI(kappa,x,y)*nx(0)*ny(1) + dx1dy0_PHI(kappa,x,y)*nx(1)*ny(0) + dx1dy1_PHI(kappa,x,y)*nx(1)*ny(1); 
    }

  public:

    int n; // number of quadrature points 
    double kappa; // wavenumber
    double eta; 
    vector <double> ts; // quadrature points; 
    bool use_deriv = false;
    vector <Complex> psi;

    KiteSolution(double akappa, int an,bool ause_deriv);
   
    // ********************* Necessary **********************************
    virtual double Evaluate(const BaseMappedIntegrationPoint& mip) const override
    {
      return 0.0;
    }

    virtual Complex EvaluateComplex(const BaseMappedIntegrationPoint& mip) const override
    {

	     
      auto pnt = mip.GetPoint();
      double r = sqrt(pnt[0]*pnt[0]+pnt[1]*pnt[1]);
      double phi = atan2(pnt[1],pnt[0]);
      
      Vector<double> x(2);
      x(0) = pnt[0];
      x(1) = pnt[1];
 

      Vector<double> nx = {x(0)/L2Norm(x),x(1)/L2Norm(x)}; 
      
      if (use_deriv) {
        
        Complex result = 0.0;
        
        for(int j=0; j<2*n;j++) {
          Vector <double> ny = { dX(ts[j])(1)/dX_r(ts[j]) ,-dX(ts[j])(0)/dX_r(ts[j])   };
	  result +=  ( dnxdny_PHI(kappa,x,X(ts[j]),nx,ny) - Complex(0,1)*eta* dnx_PHI(kappa,x,X(ts[j]),nx) )*psi[j]*dX_r(ts[j]);
        } 
	  return (M_PI/n)*result;

      }
      else {
        
	Complex result = 0.0;
        
        for(int j=0; j<2*n;j++) {
          Vector <Complex> normal = { dX(ts[j])(1)/dX_r(ts[j]) ,-dX(ts[j])(0)/dX_r(ts[j])   };
	  result += ( InnerProduct(dy_PHI(kappa,x,X(ts[j])),normal) - Complex(0,1)*eta*PHI(kappa,x,X(ts[j])) )*psi[j]*dX_r(ts[j]);
        } 
	  return (M_PI/n)*result;
      }

    }

    virtual void Evaluate(const BaseMappedIntegrationPoint& mip, FlatVector<Complex> result) const override
    {
	    Complex f = EvaluateComplex(mip);
	    result(0) = f;
    }

  };

  void ExportKiteSolution (py::module m);


  // FundamentalSolution

  class FundamentalSolution : public CoefficientFunction
  {
  public:
    double omega;
    double x_source;
    double y_source;
    bool use_deriv;
    FundamentalSolution(double aomega,double ax_source, double ay_source,bool ause_deriv )
      : CoefficientFunction(/*dimension = */ 1,true) {
        omega = aomega;
        x_source = ax_source;
        y_source = ay_source;
        use_deriv = ause_deriv;	
    }

    // ********************* Necessary **********************************
    virtual double Evaluate(const BaseMappedIntegrationPoint& mip) const override
    {
      return 0.0;
    }

    virtual Complex EvaluateComplex(const BaseMappedIntegrationPoint& mip) const override
     {
      
       auto pnt = mip.GetPoint();
       Vector<double> x(2);
       x(0) = pnt[0];
       x(1) = pnt[1];
   
       Vector<double> y(2);
       y(0) = x_source;
       y(1) = y_source;

      if (use_deriv) {
	Complex tmp = -0.25*Complex(0,1)*omega*( cyl_bessel_j(1,omega*L2Norm(x-y)) + Complex(0,1)*cyl_neumann(1, omega*L2Norm(x-y)) );
        return  tmp*InnerProduct(x,x-y)/(L2Norm(x)*L2Norm(x-y));
      }
      else{ 
         return 0.25*Complex(0,1)*( cyl_bessel_j(0,omega*L2Norm(x-y)) + Complex(0,1)*cyl_neumann(0, omega*L2Norm(x-y)) ); 
      }

     }

    virtual void Evaluate(const BaseMappedIntegrationPoint& mip, FlatVector<Complex> result) const override
    {
	    Complex f = EvaluateComplex(mip);
	    result(0) = f;
    }

  };

  void ExportFundamentalSolution (py::module m);

}

#endif //  __FILE_MYCOEFFICIENT_HPP
