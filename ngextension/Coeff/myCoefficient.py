# Test the new exported coefficientfunction

from netgen.geom2d import unit_square
from ngsolve import *
from myngspy import *

#mesh = Mesh(unit_square.GenerateMesh(maxh=0.2))

from netgen.geom2d import SplineGeometry

order = 4

geo = SplineGeometry()
geo.AddCircle( (0,0), 4.5, leftdomain=1, rightdomain=0,bc="outer-bnd")
geo.AddCircle( (0,0), 2.0, leftdomain=0, rightdomain=1,bc="inner-bnd")
geo.SetMaterial(1, "inner")
mesh = Mesh(geo.GenerateMesh (maxh=0.4,quad_dominated=False))
mesh.Curve(order)

omega = 25
n = 64

ref_sol = KiteSolution(omega,n,False)
ref_sol_dn = KiteSolution(omega,n,True)

r = sqrt(x**2+y**2)
nx = CoefficientFunction((x/r,y/r))

Draw(exp(1j*omega*x)+ref_sol,mesh,"ref")
#Draw(ref_sol_dn,mesh,"ref-dn")

print("integrate ref_sol_dn on bnd")
print("on bnd =",Integrate(ref_sol_dn,mesh,definedon=mesh.Boundaries("outer-bnd"),order=20))
print("done") 
input("")

fes = H1(mesh, complex=True,  order=order,dirichlet=[])
gfu = GridFunction (fes)
gfu.Set(ref_sol)
fem_dn = InnerProduct(grad(gfu),nx)
norm_ref = abs(sqrt(Integrate(InnerProduct(ref_sol_dn,ref_sol_dn),mesh)))
diff = sqrt(InnerProduct(fem_dn-ref_sol_dn,fem_dn-ref_sol_dn))
#Draw(diff,mesh,"diff")
print("relative L2error = ", abs(sqrt(Integrate(InnerProduct(fem_dn-ref_sol_dn,fem_dn-ref_sol_dn),mesh)))/norm_ref)


