
#include <fem.hpp>
#include <comp.hpp>
#include <python_ngstd.hpp>
#include "myCoefficient.hpp"

namespace ngfem
{
  // Export cf to Python
 
  void ExportReferenceSolution(py::module m)
  {
    py::class_<ReferenceSolution, shared_ptr<ReferenceSolution>, CoefficientFunction>
      (m, "ReferenceSolution", "Referene solution for scattering problem")
      .def(py::init<>())
      ;
  }
  
  void ExportFullSolution(py::module m)
  {

    m.def("FullSolution", [](double aomega, double aa) -> shared_ptr<CoefficientFunction>
    {
      auto FullSol = shared_ptr<CoefficientFunction>(make_shared<FullSolution> (aomega,aa));
      return FullSol;
    });

  }




  void ExportKiteSolution(py::module m)
  {
     
     /* py::class_<KiteSolution, shared_ptr<KiteSolution>, CoefficientFunction>
      (m, "KiteSolution", "Reference solution for scattering by kite shaped domain")
      .def(py::init<>())
      ; */ 
   
    m.def("KiteSolution", [](double akappa, int an,bool ause_deriv) -> shared_ptr<CoefficientFunction>
    {
      auto KiteSol = shared_ptr<CoefficientFunction>(make_shared<KiteSolution> (akappa,an,ause_deriv));
      return KiteSol;
    });

  }


  // KiteSolution  

  KiteSolution :: KiteSolution(double akappa,int an,bool ause_deriv)  : CoefficientFunction(/*dimension = */ 1,true) { 
    
    kappa = akappa; 
    n = an; 
    use_deriv = ause_deriv; 
    eta = kappa; 
    Vector<double> direc = {1.0,0.0}; 


    /* Solve boundary integral equation using Nystroem method */ 

    // right hand side for linear system;

    // Vector<double> ts(2*n);
    for(int i= 0; i < 2*n; i++) {
      ts.push_back(M_PI*i/n);
    }

    Matrix<Complex> A(2*n,2*n);
    for(int i=0;i<2*n;i++){
      for(int j=0;j<2*n;j++){
	A(i,j) = 0.0;
        if (i==j) {
	  A(i,j) += 1;
	}
	A(i,j) -= ( RR(0,ts,int(abs(i-j)),n)*K1(kappa,eta,ts[i],ts[j]) + (M_PI/n)*K2(kappa,eta,ts[i],ts[j]) );
      }
    }

    Vector<Complex> b(2*n);
    for(int i=0; i<2*n;i++) {
      b(i) =  -2*exp(Complex(0,1)*kappa*InnerProduct(direc, X(ts[i])));
    }
    CalcInverse(A);
    auto tmp = A*b;
    for(int i= 0; i<2*n;i++){
      psi.push_back(tmp(i));
    }

    
    // Calculate far field:
    Complex gamma = (M_PI/n)*exp(-Complex(0,1)*M_PI*0.25)/sqrt(8*M_PI*kappa);
    Complex result = 0.0;
    for(int j=0; j<2*n;j++) {
      Vector <Complex> normal = { dX(ts[j])(1)/dX_r(ts[j]) ,-dX(ts[j])(0)/dX_r(ts[j])   };
      result += (kappa*InnerProduct(normal,direc)+eta)*exp(-Complex(0,1)*kappa*InnerProduct(direc,X(ts[j])))*psi[j]*dX_r(ts[j]);
    }
    result *= gamma; 
    cout << "far field  = " << std::setprecision(9) << result  << endl;

  }

	

  void ExportFundamentalSolution(py::module m)
  {
     
    m.def("FundamentalSolution", [](double aomega, double ax_source, double ay_source,bool ause_deriv) -> shared_ptr<CoefficientFunction>
    {
      auto FundamentalSol = shared_ptr<CoefficientFunction>(make_shared<FundamentalSolution> (aomega,ax_source,ay_source,ause_deriv));
      return FundamentalSol;
    });

  }


  // Register cf for pickling/archiving
  // Create static object with template parameter function and base class.
  static RegisterClassForArchive<ReferenceSolution, CoefficientFunction> regRefSol;
  static RegisterClassForArchive<FullSolution, CoefficientFunction> regFullSol;
  static RegisterClassForArchive<KiteSolution, CoefficientFunction> regKiteSol;
  static RegisterClassForArchive<FundamentalSolution, CoefficientFunction> regFundSol;
}
