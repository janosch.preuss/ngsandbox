#include <solve.hpp>
using namespace ngsolve;
using namespace ngfem;
#include <python_comp.hpp>
#include "Coeff/myCoefficient.hpp"

PYBIND11_MODULE(myngspy,m) {
  // import ngsolve such that python base classes are defined
  auto ngs = py::module::import("ngsolve");
 
  ExportReferenceSolution(m);
  ExportFullSolution(m);
  ExportKiteSolution(m);
  ExportFundamentalSolution(m);
  
  // This adds documented flags to the docstring of objects.
  ngs.attr("_add_flags_doc")(m);
}

