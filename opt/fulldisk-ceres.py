import sys
sys.path.append('/home/janosch/projects/ngsandbox/opt/build')
# or append to PYTHONPATH ...
import min_dtn as m

from ngsolve import *
from netgen.geom2d import SplineGeometry
from myngspy import ReferenceSolution,FullSolution

import numpy as np
#import matplotlib.pyplot as plt
from scipy.special import hankel1,h1vp
np.random.seed(seed=123)

ngsglobals.msg_level = 0
SetNumThreads(4)

ansatz = "minimalIC"

order = 10  # order of FEM
order_geom = order
bonus_intorder = 20
dirichlet = []
solver = "pardiso" 
measure_error = True 
use_Hardy = False

rho = 1.0
c = CoefficientFunction(1)
omega = 16
Ra = 1.0

r = sqrt(x*x+y*y)

geo = SplineGeometry()
geo.AddCircle( (0,0), Ra, leftdomain=1, rightdomain=0,bc="outer-bnd")
geo.AddCircle( (0,0), 0.5, leftdomain=0, rightdomain=1,bc="inner-bnd")
geo.SetMaterial(1, "inner")
mesh = Mesh(geo.GenerateMesh (maxh=0.05,quad_dominated=False))
mesh.Curve(order)

ref_sol = FullSolution(omega,0.5)
bnd_data =  exp(1j*omega*x )*omega*1j*2*x
l2_norm =  sqrt( Integrate ( InnerProduct(ref_sol,ref_sol),mesh,VOL ).real)

Lmax = 72
lam = np.array([mm**2 for mm in range(Lmax)]) 
dtn_ref = np.array([ -omega*h1vp(j,omega*Ra) / hankel1(j,omega*Ra) for j in range(Lmax) ]) 
scale_w = 10**6
alpha_decay = 1.5
weights = np.array([scale_w*np.exp(-mm/alpha_decay) for mm in range(Lmax)])

fes_N = H1(mesh,complex=True,order=order,dirichlet=[])

l1_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
l2_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
l_dtn = m.learned_dtn(lam,dtn_ref,weights**2)
flags = {"report_level":"Brief"}
for N in range(1,7): 
    l_dtn.Run(l1_guess,l2_guess,ansatz,flags)
    print("l1 = ", l1_guess)
    print("l2 = ", l2_guess)
    l1_old = L_one = l1_guess.copy()
    l2_old = L_two = l2_guess.copy()

    if measure_error:
            
        fes_surf = H1(mesh,order=order,complex=True,definedon=mesh.Boundaries("outer-bnd"))
        inf_outer = [fes_surf for i in range(L_one.shape[0]-1)]
        
        X = FESpace([fes_N]+inf_outer)
        
        uX = X.TrialFunction() 
        vX = X.TestFunction() 
        
        f_X = LinearForm (X)
        f_X += SymbolicLFI ( bnd_data*vX[0], definedon=mesh.Boundaries("inner-bnd") ,bonus_intorder=bonus_intorder )
        f_X.Assemble()
        
        aX = BilinearForm(X, symmetric=False)
        aX += SymbolicBFI ( (1/rho)*grad(uX[0])*grad(vX[0]) - omega**2/(rho*c**2)*uX[0]*vX[0] ,bonus_intorder=bonus_intorder )
        
        for i in range(L_one.shape[0]):
            for j in range(L_one.shape[0]):
                if abs(L_one[j,i]) > 1e-10 or abs(L_two[j,i]>1e-10):
                    graduX = grad(uX[i]).Trace()
                    gradvX = grad(vX[j]).Trace()  
                    aX += SymbolicBFI ( L_two[j,i]*graduX*gradvX + L_one[j,i]*uX[i]*vX[j]  ,definedon=mesh.Boundaries("outer-bnd") ,bonus_intorder=bonus_intorder)
        aX.Assemble()
      
        
        gfu_X = GridFunction (X)
        
        res_X = gfu_X.vec.CreateVector()
        gfu_X.vec[:] = 0.0
        res_X.data = f_X.vec - aX.mat * gfu_X.vec
        Xfree = X.FreeDofs()
        invX = aX.mat.Inverse(freedofs=Xfree, inverse=solver)
        gfu_X.vec.data += invX * res_X

        l2_err =   sqrt ( Integrate (  InnerProduct(gfu_X.components[0] - ref_sol,gfu_X.components[0] - ref_sol), mesh, VOL ).real)
        print("N = {0},rel l2_err ={1} ".format(L_one.shape[0],l2_err/l2_norm))


    l1_guess = np.zeros((N+1,N+1),dtype='complex')
    l2_guess = np.zeros((N+1,N+1),dtype='complex')
    #l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    #l2_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    if ansatz in ["full","medium"]:
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = 1.0
        l1_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1))
        l2_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1))
        l1_guess[N,0] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1))
        l2_guess[N,0] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1))
    else:
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = -100-100j
        l2_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1))

