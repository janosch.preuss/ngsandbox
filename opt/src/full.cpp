// Ceres Solver - A fast non-linear least squares minimizer
// Copyright 2015 Google Inc. All rights reserved.
// http://ceres-solver.org/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// * Neither the name of Google Inc. nor the names of its contributors may be
//   used to endorse or promote products derived from this software without
//   specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: keir@google.com (Keir Mierle)
//
// A simple example of using the Ceres minimizer.
//
// Minimize 0.5 (10 - x)^2 using jacobian matrix computed using
// automatic differentiation.

#include <memory>
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/iostream.h>

//#include "min_dtn.hpp" 
#include "Eigen/Core"
// #include <vector>
// #include "Eigen/StdVector"
#include "ceres/ceres.h"
#include "ceres/loss_function.h"
#include "glog/logging.h"
#include <stdexcept>

// std
using std::shared_ptr;
// ceres 
using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;
using ceres::ScaledLoss;
using ceres::TAKE_OWNERSHIP;
// Eigen
using Eigen::Dynamic;
using Eigen::RowMajor;
typedef std::complex<double> Complex;
typedef Eigen::Matrix<Complex,Dynamic,1> ComplexVector;
typedef Eigen::Matrix<double,Dynamic,1> RealVector;
typedef Eigen::Matrix<Complex,Dynamic,Dynamic,RowMajor> ComplexMatrix;
typedef Eigen::Matrix<double,Dynamic,Dynamic,RowMajor> RealMatrix;
typedef Eigen::Matrix< std::complex<const double> ,Dynamic,Dynamic,RowMajor> ConstComplexMatrix;



struct CostFunctor {
  template <typename T> bool operator()(const T* const x, T* residual) const {
    
    Eigen::Matrix<T,1,1> A; 
    // RealMatrix A(1,1);
    A(0,0)= x[0]; 
    // residual[0] = 10.0 - x[0];
    residual[0] = 10.0 - A(0,0);
    return true;
  }
}; 


void SetSolverOptions( Solver::Options &options,pybind11::dict flags) {
  
  // setting some default values 
  options.max_num_iterations = 50000;
  options.linear_solver_type = ceres::DENSE_QR;
  options.use_nonmonotonic_steps = true;
  options.minimizer_progress_to_stdout = true;
  
  // user-defined options
  for(auto item : flags) {
    //std::cout << "key = " << std::string(pybind11::str(item.first))
    //          << ",value = " << std::string(pybind11::str(item.second)) << std::endl; 
    auto key = std::string(pybind11::str(item.first)); 
    if ( key == "max_num_iterations") {
      options.max_num_iterations = pybind11::cast<int>(item.second);
    }
    if ( key == "check_gradients") {
      options.check_gradients  = pybind11::cast<bool>(item.second);
    }
    if ( key == "use_nonmonotonic_steps") {
      options.use_nonmonotonic_steps = pybind11::cast<bool>(item.second);
    }
    if ( key == "minimizer_progress_to_stdout") {
      options.minimizer_progress_to_stdout = pybind11::cast<bool>(item.second);
    }
    if ( key == "num_threads") {
      options.num_threads = pybind11::cast<int>(item.second);
    }
    if ( key == "parameter_tolerance") {
      options.parameter_tolerance = pybind11::cast<double>(item.second);
    }
    if ( key == "function_tolerance") {
      options.function_tolerance = pybind11::cast<double>(item.second);
    }
    if ( key == "gradient_tolerance") {
      options.gradient_tolerance = pybind11::cast<double>(item.second);
    }
    if ( key == "max_solver_time_in_seconds") {
      options.max_solver_time_in_seconds = pybind11::cast<double>(item.second);
    }
  }

}



class DtnCostFunction_full : public ceres::CostFunction {
  public:
    int N;
    Complex zeta;
    double lam; 

    DtnCostFunction_full(int aN,double alam,Complex azeta) {
      N = aN;
      lam = alam;
      zeta = azeta;
      set_num_residuals(2);
      // std::cout << "lam  = " << lam << ", zeta = " << zeta << std::endl;
      for(int i = 0; i < 4; i++){
        mutable_parameter_block_sizes()->push_back(N*N);
      }
    }
    virtual ~DtnCostFunction_full() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
      
      ComplexMatrix S(N,N); 
      ComplexMatrix SE;
      ComplexMatrix SE_inv;
      
      if(N > 1) { 
        SE.resize(N-1,N-1);
      }
      

      for (int i = 0; i < N; i++){
        for(int j=  0; j < N; j++){
          S(i,j) = Complex(parameters[0][j+i*N],parameters[1][j+i*N]) + lam*Complex(parameters[2][j+i*N],parameters[3][j+i*N]);
	  if (N > 1 && i > 0 && j > 0) {
	    SE(i-1,j-1) = S(i,j);
	  }
	}
      }

      
      if( N > 1) {
	// std::cout << "SE = " << SE << std::endl;
        SE_inv = SE.inverse();
	// std::cout << "SE_inv = " << SE_inv << std::endl;
      }

      Complex zeta_approx = S(0,0);
      if (N > 1) {
        for (int i=1;i<N;i++){
          for(int j=1;j<N;j++){
            zeta_approx -= S(0,i)*SE_inv(i-1,j-1)*S(j,0);
	  }
	}
      }
      Complex diff = (zeta-zeta_approx);

      residuals[0] = diff.real();
      // std::cout << "residuals[0] = " << residuals[0] << std::endl;
      residuals[1] = diff.imag();
      
      
      // residuals[0] = 10 - L1_real;
      // residuals[1] = 5 - L1_imag;

      if (jacobians != NULL) {
       
        ComplexMatrix D_SE_inv;
        for (int k = 0; k < N; k++) {
          for(int m = 0; m < N; m++) {
            Complex d_zeta_app_km = Complex(0,0);

            if ( k > 0 && m > 0) { 
              D_SE_inv.resize(N-1,N-1);
	      for(int i = 0; i < N-1; i++) {
	        for(int j = 0; j < N-1; j++) {
	          D_SE_inv(i,j) = -SE_inv(i,k-1)*SE_inv(m-1,j);
	        }
	      }
	    }


	    if (k==0 && m ==0) {
              d_zeta_app_km = Complex(1,0);
	    }
            if (k >= 1 && m == 0) {
	      for(int i=1; i < N; i++) {
	        d_zeta_app_km -= S(0,i)*SE_inv(i-1,k-1);
	      }
	    } 
            if (k == 0 && m >= 1) {
	      for(int j=1; j < N; j++) {
	        d_zeta_app_km -= S(j,0)*SE_inv(m-1,j-1);
	      }
	    }
	    if ( k >=1 && m >= 1) { 
	      for(int i = 1; i < N; i++) {
	        for(int j = 1; j < N; j++) {
	          d_zeta_app_km -= S(0,i)*D_SE_inv(i-1,j-1)*S(j,0);
	        }
	      }
	    }

            if (jacobians[0] != NULL) {
              jacobians[0][m+k*N] = -d_zeta_app_km.real();
              jacobians[0][m+k*N+N*N] = -d_zeta_app_km.imag();
            }
            if (jacobians[1] != NULL) {
              jacobians[1][m+k*N] = -(Complex(0,1)*d_zeta_app_km).real();
              jacobians[1][m+k*N+N*N] = -(Complex(0,1)*d_zeta_app_km).imag();
            }
            if (jacobians[2] != NULL) {
              jacobians[2][m+k*N] = -(lam*d_zeta_app_km).real();
              jacobians[2][m+k*N+N*N] = -(lam*d_zeta_app_km).imag();
            }
            if (jacobians[3] != NULL) {
              jacobians[3][m+k*N] = -(Complex(0,1)*lam*d_zeta_app_km).real();
              jacobians[3][m+k*N+N*N] = -(Complex(0,1)*lam*d_zeta_app_km).imag();
            }

	  }
        }
         

      }

      return true;
    }
};


class DtnCostFunction_medium : public ceres::CostFunction {
  public:
    int N;
    Complex zeta;
    double lam; 

    DtnCostFunction_medium(int aN,double alam,Complex azeta) {
      N = aN;
      lam = alam;
      zeta = azeta;
      set_num_residuals(2); // Real and Imaginary part
      mutable_parameter_block_sizes()->push_back(4); // II-block
      if ( N > 1) { // IE,EI and EE-blocks
        for (int j=0; j < 3; j++) {
          mutable_parameter_block_sizes()->push_back(4*(N-1)); 
        }
      }
    }
    virtual ~DtnCostFunction_medium() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
     Complex zeta_approx = Complex(parameters[0][0],parameters[0][1]) + lam*Complex(parameters[0][2],parameters[0][3]);
      
      if (N > 1) {
        for(int j=0;j<N-1;j++){
          Complex S_IE_j = Complex(parameters[1][j],parameters[1][j+N-1]) + lam*Complex(parameters[1][j+2*(N-1)],parameters[1][j+3*(N-1)]);
          Complex S_EI_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam*Complex(parameters[2][j+2*(N-1)],parameters[2][j+3*(N-1)]);
          Complex S_EE_j = Complex(parameters[3][j],parameters[3][j+N-1]) + lam*Complex(parameters[3][j+2*(N-1)],parameters[3][j+3*(N-1)]);
          zeta_approx -= S_IE_j*S_EI_j/S_EE_j;
	}	
      }

      Complex diff = (zeta-zeta_approx);
      residuals[0] = diff.real();
      // std::cout << "residuals[0] = " << residuals[0] << std::endl;
      residuals[1] = diff.imag();
            
      
      if (jacobians != NULL) {

	// II-block
        if (jacobians[0] != NULL) {
	  jacobians[0][0] = -1; // real part 
	  jacobians[0][0+4] = 0; // imaginary part
	  jacobians[0][1] = 0;
	  jacobians[0][1+4] = -1;
	  jacobians[0][2] = -lam;
	  jacobians[0][2+4] = 0;
	  jacobians[0][3] = 0;
	  jacobians[0][3+4] = -lam;
        }

        int offset = 4*(N-1); // for residual[1]

	for (int j =0; j < N-1; j++) {
          
	  Complex S_IE_j = Complex(parameters[1][j],parameters[1][j+N-1]) + lam*Complex(parameters[1][j+2*(N-1)],parameters[1][j+3*(N-1)]);
          Complex S_EI_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam*Complex(parameters[2][j+2*(N-1)],parameters[2][j+3*(N-1)]);
          Complex S_EE_j = Complex(parameters[3][j],parameters[3][j+N-1]) + lam*Complex(parameters[3][j+2*(N-1)],parameters[3][j+3*(N-1)]);
	  
	  Complex d_L1_IE_real_j = (S_EI_j/S_EE_j); 
	  Complex d_L1_EI_real_j = (S_IE_j/S_EE_j);
	  Complex d_L1_EE_real_j = -(S_IE_j*S_EI_j/(S_EE_j*S_EE_j));

	  // IE-block
          if (jacobians[1] != NULL) {

	    jacobians[1][j] = d_L1_IE_real_j.real(); 
	    jacobians[1][j+offset] = d_L1_IE_real_j.imag(); 

	    jacobians[1][j+(N-1)] = -d_L1_IE_real_j.imag(); 
	    jacobians[1][j+(N-1)+offset] = d_L1_IE_real_j.real(); 
	    
	    jacobians[1][j+2*(N-1)] = lam*d_L1_IE_real_j.real(); 
	    jacobians[1][j+2*(N-1)+offset] = lam*d_L1_IE_real_j.imag(); 
	    
	    jacobians[1][j+3*(N-1)] = -lam*d_L1_IE_real_j.imag(); 
	    jacobians[1][j+3*(N-1)+offset] = lam*d_L1_IE_real_j.real(); 
          }  

	  // EI-block
          if (jacobians[2] != NULL) {
	    jacobians[2][j] = d_L1_EI_real_j.real(); 
	    jacobians[2][j+offset] = d_L1_EI_real_j.imag();
	    
	    jacobians[2][j+(N-1)] = -d_L1_EI_real_j.imag(); 
	    jacobians[2][j+(N-1)+offset] = d_L1_EI_real_j.real(); 
	    
	    jacobians[2][j+2*(N-1)] = lam*d_L1_EI_real_j.real(); 
	    jacobians[2][j+2*(N-1)+offset] = lam*d_L1_EI_real_j.imag(); 
	    
	    jacobians[2][j+3*(N-1)] = -lam*d_L1_EI_real_j.imag(); 
	    jacobians[2][j+3*(N-1)+offset] = lam*d_L1_EI_real_j.real(); 
          }  
	
	  // EE-block
          if (jacobians[3] != NULL) {
	    jacobians[3][j] = d_L1_EE_real_j.real(); 
	    jacobians[3][j+offset] = d_L1_EE_real_j.imag();

	    jacobians[3][j+(N-1)] = -d_L1_EE_real_j.imag(); 
	    jacobians[3][j+(N-1)+offset] = d_L1_EE_real_j.real(); 

	    jacobians[3][j+2*(N-1)] = lam*d_L1_EE_real_j.real(); 
	    jacobians[3][j+2*(N-1)+offset] = lam*d_L1_EE_real_j.imag(); 
	    
	    jacobians[3][j+3*(N-1)] = -lam*d_L1_EE_real_j.imag(); 
	    jacobians[3][j+3*(N-1)+offset] = lam*d_L1_EE_real_j.real(); 
          }  

	}
	
      }

      return true;
    }
};

class DtnCostFunction_minimalIC : public ceres::CostFunction {
  public:
    int N;
    Complex zeta;
    double lam; 

    DtnCostFunction_minimalIC(int aN,double alam,Complex azeta) {
      N = aN;
      lam = alam;
      zeta = azeta;
      set_num_residuals(2); // Real and Imaginary part
      mutable_parameter_block_sizes()->push_back(4); // II-block
      if (N > 1) {
        mutable_parameter_block_sizes()->push_back(4*(N-1)); // IE-block
        mutable_parameter_block_sizes()->push_back(2*(N-1)); // EI-block
        mutable_parameter_block_sizes()->push_back(2*(N-1)); // EE-block
      }
    }
    virtual ~DtnCostFunction_minimalIC() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
     Complex zeta_approx = Complex(parameters[0][0],parameters[0][1]) + lam*Complex(parameters[0][2],parameters[0][3]);
      
      if (N > 1) {
        for(int j=0;j<N-1;j++){
          Complex S_IE_j = Complex(parameters[1][j],parameters[1][j+N-1]) + lam*Complex(parameters[1][j+2*(N-1)],parameters[1][j+3*(N-1)]);
          Complex S_EI_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam;
          Complex S_EE_j = Complex(parameters[3][j],parameters[3][j+N-1]) + lam;
          zeta_approx -= S_IE_j*S_EI_j/S_EE_j;
	}	
      }

      Complex diff = (zeta-zeta_approx);
      residuals[0] = diff.real();
      // std::cout << "residuals[0] = " << residuals[0] << std::endl;
      residuals[1] = diff.imag();
            
      
      if (jacobians != NULL) {

	// II-block
        if (jacobians[0] != NULL) {
	  jacobians[0][0] = -1; // real part 
	  jacobians[0][0+4] = 0; // imaginary part
	  jacobians[0][1] = 0;
	  jacobians[0][1+4] = -1;
	  jacobians[0][2] = -lam;
	  jacobians[0][2+4] = 0;
	  jacobians[0][3] = 0;
	  jacobians[0][3+4] = -lam;
        }

        int offset1 = 4*(N-1); // for residual[1]
        int offset2 = 2*(N-1); // for residual[2],residual[3]

	for (int j =0; j < N-1; j++) {
          
	  Complex S_IE_j = Complex(parameters[1][j],parameters[1][j+N-1]) + lam*Complex(parameters[1][j+2*(N-1)],parameters[1][j+3*(N-1)]);
          Complex S_EI_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam;
          Complex S_EE_j = Complex(parameters[3][j],parameters[3][j+N-1]) + lam;
	  
	  Complex d_L1_IE_real_j = (S_EI_j/S_EE_j); 
	  Complex d_L1_EI_real_j = (S_IE_j/S_EE_j);
	  Complex d_L1_EE_real_j = -(S_IE_j*S_EI_j/(S_EE_j*S_EE_j));

	  // IE-block
          if (jacobians[1] != NULL) {

	    jacobians[1][j] = d_L1_IE_real_j.real(); 
	    jacobians[1][j+offset1] = d_L1_IE_real_j.imag(); 

	    jacobians[1][j+(N-1)] = -d_L1_IE_real_j.imag(); 
	    jacobians[1][j+(N-1)+offset1] = d_L1_IE_real_j.real(); 
	    
	    jacobians[1][j+2*(N-1)] = lam*d_L1_IE_real_j.real(); 
	    jacobians[1][j+2*(N-1)+offset1] = lam*d_L1_IE_real_j.imag(); 
	    
	    jacobians[1][j+3*(N-1)] = -lam*d_L1_IE_real_j.imag(); 
	    jacobians[1][j+3*(N-1)+offset1] = lam*d_L1_IE_real_j.real(); 
          }  

	  // EI-block
          if (jacobians[2] != NULL) {
	    jacobians[2][j] = d_L1_EI_real_j.real(); 
	    jacobians[2][j+offset2] = d_L1_EI_real_j.imag();
	    
	    jacobians[2][j+(N-1)] = -d_L1_EI_real_j.imag(); 
	    jacobians[2][j+(N-1)+offset2] = d_L1_EI_real_j.real(); 
	    
          }  
	
	  // EE-block
          if (jacobians[3] != NULL) {
	    jacobians[3][j] = d_L1_EE_real_j.real(); 
	    jacobians[3][j+offset2] = d_L1_EE_real_j.imag();

	    jacobians[3][j+(N-1)] = -d_L1_EE_real_j.imag(); 
	    jacobians[3][j+(N-1)+offset2] = d_L1_EE_real_j.real(); 

          }  

	}
	
      }

      return true;
    }
};


class DtnCostFunction_minimal : public ceres::CostFunction {
  public:
    int N;
    Complex zeta;
    double lam; 

    DtnCostFunction_minimal(int aN,double alam,Complex azeta) {
      N = aN;
      lam = alam;
      zeta = azeta;
      set_num_residuals(2); // Real and Imaginary part
      mutable_parameter_block_sizes()->push_back(4); // II-block
      if ( N > 1) {
	// IE-block
        mutable_parameter_block_sizes()->push_back(2*(N-1));
        // EE-block
	mutable_parameter_block_sizes()->push_back(2*(N-1));
      }
    }
    virtual ~DtnCostFunction_minimal() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
     Complex zeta_approx = Complex(parameters[0][0],parameters[0][1]) + lam*Complex(parameters[0][2],parameters[0][3]);
      
      if (N > 1) {
        for(int j=0;j<N-1;j++){
          Complex S_IE_squared_j = Complex(parameters[1][j],parameters[1][j+N-1]);
          Complex S_EE_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam;
          zeta_approx -= S_IE_squared_j/S_EE_j;
	}	
      }

      Complex diff = (zeta-zeta_approx);
      residuals[0] = diff.real();
      // std::cout << "residuals[0] = " << residuals[0] << std::endl;
      residuals[1] = diff.imag();
            
      
      if (jacobians != NULL) {

	// II-block
        if (jacobians[0] != NULL) {
	  jacobians[0][0] = -1; // real part 
	  jacobians[0][0+4] = 0; // imaginary part
	  jacobians[0][1] = 0;
	  jacobians[0][1+4] = -1;
	  jacobians[0][2] = -lam;
	  jacobians[0][2+4] = 0;
	  jacobians[0][3] = 0;
	  jacobians[0][3+4] = -lam;
        }

        int offset = 2*(N-1); // for residual[1]

	for (int j =0; j < N-1; j++) {
          
          Complex S_IE_squared_j = Complex(parameters[1][j],parameters[1][j+N-1]);
          Complex S_EE_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam;
	  
	  Complex d_L1_IE_real_j = 1.0/S_EE_j; 
	  Complex d_L1_EE_real_j = -(S_IE_squared_j/(S_EE_j*S_EE_j));

	  // IE-block
          if (jacobians[1] != NULL) {

	    jacobians[1][j] = d_L1_IE_real_j.real(); 
	    jacobians[1][j+offset] = d_L1_IE_real_j.imag(); 

	    jacobians[1][j+(N-1)] = -d_L1_IE_real_j.imag(); 
	    jacobians[1][j+(N-1)+offset] = d_L1_IE_real_j.real(); 
	    
          }  

	  // EE-block
          if (jacobians[2] != NULL) {
	    jacobians[2][j] = d_L1_EE_real_j.real(); 
	    jacobians[2][j+offset] = d_L1_EE_real_j.imag();

	    jacobians[2][j+(N-1)] = -d_L1_EE_real_j.imag(); 
	    jacobians[2][j+(N-1)+offset] = d_L1_EE_real_j.real(); 

          }  

	}
	
      }

      return true;
    }
};






double solve_problem(double x0) {

  double x = x0;
  const double initial_x = x;
  Problem problem; 
   CostFunction* cost_function =
      new AutoDiffCostFunction<CostFunctor, 1, 1>(new CostFunctor);
  // CostFunction* cost_function =
  //     new DtnCostFunction; 
  problem.AddResidualBlock(cost_function, NULL, &x); 
  Solver::Options options;
  options.minimizer_progress_to_stdout = true;
  options.check_gradients = true;
  Solver::Summary summary;
  Solve(options, &problem, &summary);

  std::cout << summary.BriefReport() << "\n";
  std::cout << "x : " << initial_x
            << " -> " << x << "\n";

  return x;
}


class learned_dtn
{
  protected:

  public:
    RealVector lam; // eigenvalues
    ComplexVector zeta_ref; // dtn numbers 
    RealVector weight; // weights
    
    learned_dtn(RealVector alam,ComplexVector azeta_ref,RealVector aweight)  { 
      lam = alam; 
      zeta_ref = azeta_ref;     
      weight = aweight;    
    }
      
    void Run_full( Eigen::Ref<ComplexMatrix> Lone_guess,
	           Eigen::Ref<ComplexMatrix> Ltwo_guess,
		   pybind11::dict flags,
		   Eigen::Ref<RealVector> final_res
                 ) { 

	int N = Lone_guess.cols();
	
	RealVector L1_real(N*N);
	RealVector L1_imag(N*N);
	RealVector L2_real(N*N);
	RealVector L2_imag(N*N);

        for (int i = 0; i < N; i++) {
          for(int j= 0; j < N; j++) {
	    L1_real(j+i*N) = Lone_guess.real()(i,j);
	    L1_imag(j+i*N) = Lone_guess.imag()(i,j);
	    L2_real(j+i*N) = Ltwo_guess.real()(i,j);
	    L2_imag(j+i*N) = Ltwo_guess.imag()(i,j);
	  }
	}


	Problem problem; 
        for(int i = 0; i < lam.size(); i++) {	
	  CostFunction* cost_function =
	    new DtnCostFunction_full(N,lam[i],zeta_ref[i]); 
	  problem.AddResidualBlock(cost_function, new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP), L1_real.data(),L1_imag.data(),L2_real.data(),L2_imag.data()); 
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);

	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	}

	if( final_res.size() == lam.size()) {
          auto evopt = Problem::EvaluateOptions();
          evopt.apply_loss_function = false;
	  std::vector<double> afinal_res;
	  problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	  int k = 0;
	  int kk = 0;
	  while(kk < afinal_res.size()-1) {
	    final_res(k) = sqrt(afinal_res[kk]*afinal_res[kk] + afinal_res[kk+1]*afinal_res[kk+1])/abs(zeta_ref(k));
	    k += 1;
	    kk += 2;
	  }
	}
        
	for (int i = 0; i < N; i++) {
          for(int j= 0; j < N; j++) {
	    Lone_guess(i,j) = Complex(L1_real(j+i*N),L1_imag(j+i*N));
	    Ltwo_guess(i,j) = Complex(L2_real(j+i*N),L2_imag(j+i*N));
	  }
	}
    }

    void Run_medium( Eigen::Ref<ComplexMatrix> Lone_guess,
	             Eigen::Ref<ComplexMatrix> Ltwo_guess,
		     pybind11::dict flags,
		     Eigen::Ref<RealVector> final_res
                   ) { 

	int N = Lone_guess.cols();
	
	// ordering of vectors:
	// S_II = [Re(Lone),Im(Lone),Re(Ltwo),Im(Ltwo)] 
	
	RealVector S_II(4);
        
	S_II(0) = Lone_guess.real()(0,0);
        S_II(1) = Lone_guess.imag()(0,0);
        S_II(2) = Ltwo_guess.real()(0,0);
        S_II(3) = Ltwo_guess.imag()(0,0);

	RealVector S_IE;
	RealVector S_EI;
	RealVector S_EE;

        if ( N > 1) {
	  
	  S_IE.resize(4*(N-1));
	  S_EI.resize(4*(N-1));
	  S_EE.resize(4*(N-1));
	  
	  for(int i=1; i <N; i++) {

	    S_IE(i-1)           =  Lone_guess.real()(0,i);
	    S_IE(i-1 + N-1)     =  Lone_guess.imag()(0,i);
	    S_IE(i-1 + 2*(N-1)) =  Ltwo_guess.real()(0,i);
	    S_IE(i-1 + 3*(N-1)) =  Ltwo_guess.imag()(0,i);

	    S_EI(i-1)           =  Lone_guess.real()(i,0);
	    S_EI(i-1 + N-1)     =  Lone_guess.imag()(i,0);
	    S_EI(i-1 + 2*(N-1)) =  Ltwo_guess.real()(i,0);
	    S_EI(i-1 + 3*(N-1)) =  Ltwo_guess.imag()(i,0);

	    S_EE(i-1)           =  Lone_guess.real()(i,i);
	    S_EE(i-1 + N-1 )    =  Lone_guess.imag()(i,i);
	    S_EE(i-1 +2*(N-1))  =  Ltwo_guess.real()(i,i);
	    S_EE(i-1 +3*(N-1))  =  Ltwo_guess.imag()(i,i);
	  
	  }

	}	



	Problem problem; 
        for(int i = 0; i < lam.size(); i++) {	
	  CostFunction* cost_function =
	    new DtnCostFunction_medium(N,lam[i],zeta_ref[i]); 
	  if (N > 1) {
	    problem.AddResidualBlock(cost_function,new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP), 
			             S_II.data(),S_IE.data(),S_EI.data(),S_EE.data() 
				    );
	  } 
	  else {
	    problem.AddResidualBlock(cost_function,new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP),S_II.data());
	  }
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);
       
	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	}

	if( final_res.size() == lam.size()) {
          auto evopt = Problem::EvaluateOptions();
          evopt.apply_loss_function = false;
	  std::vector<double> afinal_res;
	  problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	  int k = 0;
	  int kk = 0;
	  while(kk < afinal_res.size()-1) {
	    final_res(k) = sqrt(afinal_res[kk]*afinal_res[kk] + afinal_res[kk+1]*afinal_res[kk+1])/abs(zeta_ref(k));
	    k += 1;
	    kk += 2;
	  }
	}

	Lone_guess.setZero();
        Ltwo_guess.setZero();

        Lone_guess.real()(0,0) = S_II(0);
        Lone_guess.imag()(0,0) = S_II(1);
        Ltwo_guess.real()(0,0) = S_II(2);
        Ltwo_guess.imag()(0,0) = S_II(3);

	if ( N > 1) {
         
          for(int i = 1; i < N; i++){
		
	    Lone_guess.real()(0,i) = S_IE(i-1);
	    Lone_guess.imag()(0,i) = S_IE(i-1 + N-1);
	    Ltwo_guess.real()(0,i) = S_IE(i-1 +2*(N-1)); 
	    Ltwo_guess.imag()(0,i) = S_IE(i-1 +3*(N-1)); 

	    Lone_guess.real()(i,0) = S_EI(i-1); 
	    Lone_guess.imag()(i,0) = S_EI(i-1 + N-1); 
	    Ltwo_guess.real()(i,0) = S_EI(i-1 +2*(N-1)); 
	    Ltwo_guess.imag()(i,0) = S_EI(i-1 +3*(N-1));

	    Lone_guess.real()(i,i) = S_EE(i-1);
	    Lone_guess.imag()(i,i) = S_EE(i-1 + (N-1));
	    Ltwo_guess.real()(i,i) = S_EE(i-1 +2*(N-1));
	    Ltwo_guess.imag()(i,i) = S_EE(i-1 +3*(N-1));

	  }
	}

    }

    void Run_minimal( Eigen::Ref<ComplexMatrix> Lone_guess,
	              Eigen::Ref<ComplexMatrix> Ltwo_guess,
		      pybind11::dict flags,
		      Eigen::Ref<RealVector> final_res
		    ) { 

	int N = Lone_guess.cols();
	
	// ordering of vectors:
	// S_II = [Re(Lone_II),Im(Lone_II),Re(Ltwo_II),Im(Ltwo_II)] 
	
	RealVector S_II(4);
        
	S_II(0) = Lone_guess.real()(0,0);
        S_II(1) = Lone_guess.imag()(0,0);
        S_II(2) = Ltwo_guess.real()(0,0);
        S_II(3) = Ltwo_guess.imag()(0,0);

	// S_IE = SE_I, S_IE_squared = S_IE*S_EI
	RealVector S_IE_squared; // 
	// S_EE = [Re(Lone_EE),Im(Lone_EE)]
	RealVector S_EE;

        if ( N > 1) {
	  
	  S_IE_squared.resize(2*(N-1));
	  S_EE.resize(2*(N-1));
	  
	  for(int i=1; i <N; i++) {
            Complex tmp = Lone_guess(0,i)*Lone_guess(i,0);
	    S_IE_squared(i-1)       =  tmp.real();
	    S_IE_squared(i-1 + N-1) =  tmp.imag();

	    S_EE(i-1)           =  Lone_guess.real()(i,i);
	    S_EE(i-1 + N-1 )    =  Lone_guess.imag()(i,i);
	  }
	}	

	Problem problem; 
        for(int i = 0; i < lam.size(); i++) {	
	  CostFunction* cost_function =
	    new DtnCostFunction_minimal(N,lam[i],zeta_ref[i]); 
	  if (N > 1) {
	    problem.AddResidualBlock(cost_function, new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP), 
			             S_II.data(),S_IE_squared.data(),S_EE.data() 
				    );
	  } 
	  else {
	    problem.AddResidualBlock(cost_function, new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP),S_II.data());
	  }
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);       

	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	}

	if( final_res.size() == lam.size()) {
          auto evopt = Problem::EvaluateOptions();
          evopt.apply_loss_function = false;
	  std::vector<double> afinal_res;
	  problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	  int k = 0;
	  int kk = 0;
	  while(kk < afinal_res.size()-1) {
	    final_res(k) = sqrt(afinal_res[kk]*afinal_res[kk] + afinal_res[kk+1]*afinal_res[kk+1])/abs(zeta_ref(k));
	    k += 1;
	    kk += 2;
	  }
	}

        Lone_guess.setZero();
        Ltwo_guess.setZero();
	
	Lone_guess.real()(0,0) = S_II(0);
        Lone_guess.imag()(0,0) = S_II(1);
        Ltwo_guess.real()(0,0) = S_II(2);
        Ltwo_guess.imag()(0,0) = S_II(3);

	if ( N > 1) {
         
          for(int i = 1; i < N; i++){
		
	    Lone_guess(0,i) = std::sqrt(Complex(S_IE_squared(i-1),S_IE_squared(i-1 + N-1))); 
	    Lone_guess(i,0) = std::sqrt(Complex(S_IE_squared(i-1),S_IE_squared(i-1 + N-1)));
	     
	    Lone_guess.real()(i,i) = S_EE(i-1);
	    Lone_guess.imag()(i,i) = S_EE(i-1 + (N-1));
	    Ltwo_guess(i,i) = Complex(1,0);

	  }
	}
    
    }

    void Run_minimalIC( Eigen::Ref<ComplexMatrix> Lone_guess,
	                Eigen::Ref<ComplexMatrix> Ltwo_guess,
			pybind11::dict flags,
		        Eigen::Ref<RealVector> final_res
		      ) { 

	int N = Lone_guess.cols();
	
	// ordering of vectors:
	// S_II = [Re(Lone),Im(Lone),Re(Ltwo),Im(Ltwo)] 
	
	RealVector S_II(4);
        
	S_II(0) = Lone_guess.real()(0,0);
        S_II(1) = Lone_guess.imag()(0,0);
        S_II(2) = Ltwo_guess.real()(0,0);
        S_II(3) = Ltwo_guess.imag()(0,0);

	RealVector S_IE;
	RealVector S_EI;
	RealVector S_EE;

        if ( N > 1) {
	  
	  S_IE.resize(4*(N-1));
	  S_EI.resize(2*(N-1));
	  S_EE.resize(2*(N-1));
	  
	  for(int i=1; i <N; i++) {

	    S_IE(i-1)           =  Lone_guess.real()(0,i);
	    S_IE(i-1 + N-1)     =  Lone_guess.imag()(0,i);
	    S_IE(i-1 + 2*(N-1)) =  Ltwo_guess.real()(0,i);
	    S_IE(i-1 + 3*(N-1)) =  Ltwo_guess.imag()(0,i);

	    S_EI(i-1)           =  Lone_guess.real()(i,0);
	    S_EI(i-1 + N-1)     =  Lone_guess.imag()(i,0);

	    S_EE(i-1)           =  Lone_guess.real()(i,i);
	    S_EE(i-1 + N-1 )    =  Lone_guess.imag()(i,i);
	  
	  }

	}	

	Problem problem; 
        for(int i = 0; i < lam.size(); i++) {	
	  CostFunction* cost_function =
	    new DtnCostFunction_minimalIC(N,lam[i],zeta_ref[i]); 
	  if (N > 1) {
	    problem.AddResidualBlock(cost_function,new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP) , 
			             S_II.data(),S_IE.data(),S_EI.data(),S_EE.data() 
				    );
	  } 
	  else {
	    problem.AddResidualBlock(cost_function,new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP),S_II.data());
	  }
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);


	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	}

	if( final_res.size() == lam.size()) {
          auto evopt = Problem::EvaluateOptions();
          evopt.apply_loss_function = false;
	  std::vector<double> afinal_res;
	  problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	  int k = 0;
	  int kk = 0;
	  while(kk < afinal_res.size()-1) {
	    final_res(k) = sqrt(afinal_res[kk]*afinal_res[kk] + afinal_res[kk+1]*afinal_res[kk+1])/abs(zeta_ref(k));
	    k += 1;
	    kk += 2;
	  }
	}

        Lone_guess.setZero();
        Ltwo_guess.setZero();

        Lone_guess.real()(0,0) = S_II(0);
        Lone_guess.imag()(0,0) = S_II(1);
        Ltwo_guess.real()(0,0) = S_II(2);
        Ltwo_guess.imag()(0,0) = S_II(3);

	if ( N > 1) {
         
          for(int i = 1; i < N; i++){
		
	    Lone_guess.real()(0,i) = S_IE(i-1);
	    Lone_guess.imag()(0,i) = S_IE(i-1 + N-1);
	    Ltwo_guess.real()(0,i) = S_IE(i-1 +2*(N-1)); 
	    Ltwo_guess.imag()(0,i) = S_IE(i-1 +3*(N-1)); 

	    Lone_guess.real()(i,0) = S_EI(i-1); 
	    Lone_guess.imag()(i,0) = S_EI(i-1 + N-1); 
	    Ltwo_guess(i,0) = Complex(1,0); 

	    Lone_guess.real()(i,i) = S_EE(i-1);
	    Lone_guess.imag()(i,i) = S_EE(i-1 + (N-1));
	    Ltwo_guess(i,i) = Complex(1,0);

	  }
	}
    }

}; 



void eval_dtn_fct(Eigen::Ref<ComplexMatrix> Lone,
	          Eigen::Ref<ComplexMatrix> Ltwo,
		  Eigen::Ref<RealVector> lam,
		  Eigen::Ref<ComplexVector> val
		) 
{
  int N = Lone.cols();
  
  ComplexMatrix S(N,N); 
  ComplexMatrix SE;
  ComplexMatrix SE_inv;
  
  if(N > 1) { 
    SE.resize(N-1,N-1);
  }

  for(int k=0; k < lam.size(); k++) {
 
    for (int i = 0; i < N; i++){
      for(int j=  0; j < N; j++){
        S(i,j) = Lone(i,j) + lam(k)*Ltwo(i,j);
	if (N > 1 && i > 0 && j > 0) {
	  SE(i-1,j-1) = S(i,j);
	}
      }
    }
      
    if( N > 1) {
      SE_inv = SE.inverse();
    }

    Complex zeta_approx = S(0,0);
    if (N > 1) {
      for (int i=1;i<N;i++){
        for(int j=1;j<N;j++){
          zeta_approx -= S(0,i)*SE_inv(i-1,j-1)*S(j,0);
	  }
	}
      }
      val(k) = zeta_approx;	
  }
      
}




namespace py = pybind11;

PYBIND11_MODULE(min_dtn, m) {
    m.doc() = R"pbdoc(
        Pybind11 example plugin
        -----------------------
        .. currentmodule:: cmake_example
        .. autosummary::
           :toctree: _generate
    )pbdoc";

    m.def("solve_problem", &solve_problem, R"pbdoc(
        Some doctring
    )pbdoc");

    py::class_<learned_dtn, shared_ptr<learned_dtn>>
      (m,"learned_dtn","Learned DtN")
      //.def(py::init<int,int>())
      .def(py::init<RealVector,ComplexVector,RealVector>())
      .def("Run",[] (learned_dtn self,
		     Eigen::Ref<ComplexMatrix> Lone,
		     Eigen::Ref<ComplexMatrix> Ltwo,
		     const std::string & ansatz,
		     py::dict flags,
		     Eigen::Ref<RealVector> final_res
			      ) {
	if( Lone.rows() != Lone.cols()) {
	  throw std::invalid_argument("Lone has to be a quadratic matrix!");
	}
        
	py::scoped_ostream_redirect stream(
	  std::cout,
	  py::module::import("sys").attr("stdout")
	);
        	
	if ( ansatz == "minimal") {
	   self.Run_minimal(Lone,Ltwo,flags,final_res);
	}
	else {
	      if ( ansatz  == "medium" ) {
	        self.Run_medium(Lone,Ltwo,flags,final_res);
	      }
	      else {
	            if ( ansatz == "minimalIC") {
	              self.Run_minimalIC(Lone,Ltwo,flags,final_res);
		   } 
	            else {	
	              self.Run_full(Lone,Ltwo,flags,final_res);
	      }
	    }
	}
      },
      py::arg("Lone"),py::arg("Ltwo"),py::arg("ansatz")="full",
      py::arg("flags")=py::dict(),
      py::arg("final_res")=RealVector(1) )
      ;

      
      m.def("eval_dtn_fct",[] ( Eigen::Ref<ComplexMatrix> Lone,
		                Eigen::Ref<ComplexMatrix> Ltwo,
				Eigen::Ref<RealVector> lam,
				Eigen::Ref<ComplexVector> val
			       ) {
        eval_dtn_fct(Lone,Ltwo,lam,val);
      }, 
      py::arg("Lone"),py::arg("Ltwo"),py::arg("lam"),py::arg("val")
      ); 
      

#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif
}

